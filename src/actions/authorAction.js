let nextAuthorId = 0

export const addAuthor = text => ({
  type: 'ADD_AUTHOR',
  id: nextAuthorId++,
  text
})

export const setVisibilityFilter = filter => ({
  type: 'SET_VISIBILITY_FILTER',
  filter
})

export const toggleAuthor = id => ({
  type: 'TOGGLE_AUTHOR',
  id
})

export const VisibilityFilters = {
  SHOW_ALL: 'SHOW_ALL',
  SHOW_COMPLETED: 'SHOW_COMPLETED',
  SHOW_ACTIVE: 'SHOW_ACTIVE'
}
import React from 'react'
import { connect } from 'react-redux'
import { addAuthor } from '/home/talmeid/Projetos/library/src/actions/authorAction'

const AddAuthor = ({ dispatch }) => {
  let input

  return (
    <div>
      <form
        onSubmit={e => {
          e.preventDefault()
          if (!input.value.trim()) {
            return
          }
          dispatch(addAuthor(input.value))
          input.value = ''
        }}
      >
        <input ref={node => input = node} />
        <button type="submit">
          Add New Author
        </button>
      </form>
    </div>
  )
}

export default connect()(AddAuthor)
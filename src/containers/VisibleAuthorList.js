import { connect } from 'react-redux'
import { toggleAuthor } from '/home/talmeid/Projetos/library/src/actions/authorAction.js'
import AuthorList from '../components/AllAuthors'
import { VisibilityFilters } from '/home/talmeid/Projetos/library/src/actions/authorAction'

const getVisibleAuthors = (authors, filter) => {
  switch (filter) {
    case VisibilityFilters.SHOW_ALL:
      return authors
    case VisibilityFilters.SHOW_COMPLETED:
      return authors.filter(t => t.completed)
    case VisibilityFilters.SHOW_ACTIVE:
      return authors.filter(t => !t.completed)
    default:
      throw new Error('Unknown filter: ' + filter)
  }
}

const mapStateToProps = state => ({
  authors: getVisibleAuthors(state.authors, state.visibilityFilter)
})

const mapDispatchToProps = dispatch => ({
  toggleAuthor: id => dispatch(toggleAuthor(id))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AuthorList)
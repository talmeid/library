import React from 'react'
import Footer from './Footer'
import AddAuthor from '../containers/AddAuthor'
import VisibleAuthorList from '../containers/VisibleAuthorList'

const App = () => (
  <div>
    <AddAuthor />
    <VisibleAuthorList />
    <Footer />
  </div>
)

export default App
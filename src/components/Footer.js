import React from 'react'
import FilterLink from '../containers/FilterLink'
import { VisibilityFilters } from '/home/talmeid/Projetos/library/src/actions/authorAction'

const Footer = () => (
  <div>
    <span>Show: </span>
    <FilterLink filter={VisibilityFilters.SHOW_ALL}>
      All
    </FilterLink>
  </div>
)

export default Footer
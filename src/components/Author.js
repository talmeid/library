import React from 'react'
import PropTypes from 'prop-types'

const Author = ({ onClick, firstName, lastName}) => (
  <li
    onClick={onClick}
  >
    {firstName} {lastName}
  </li>
)

Author.propTypes = {
  onClick: PropTypes.func.isRequired,
  firstName: PropTypes.string.isRequired,
  lastName: PropTypes.string.isRequired
}

export default Author
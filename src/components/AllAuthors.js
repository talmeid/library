import PropTypes from 'prop-types'
import Author from './Author'
import axios from 'axios'


import React, { Component } from 'react';
import { connect } from 'react-redux';

import { toggleAuthor } from '/home/talmeid/Projetos/library/src/actions/authorAction.js'

class AuthorList extends Component {

  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      authors: []
    };
  }

  componentDidMount() {
    axios.get("https://bibliapp.herokuapp.com/api/authors")
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            authors: result.data
          });
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
  }

  render() {
    const { authors } = this.state;
    return (
      <ul>
        {authors.map(author =>
      <Author
        key={author.id}
        {...author}
        onClick={() => toggleAuthor(author.id)}
      />
    )}
      </ul>
    );
  }
}

const mapStateToProps = (state) => {
return {
posts: state
}
}

AuthorList.propTypes = {
  authors: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      completed: PropTypes.bool.isRequired,
      text: PropTypes.string.isRequired
    }).isRequired
  ).isRequired,
  toggleAuthor: PropTypes.func.isRequired
}

export default connect(mapStateToProps)(AuthorList);
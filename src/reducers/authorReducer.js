const authorReducer = (state = [], action) => {
  switch (action.type) {
    case 'ADD_AUTHOR':
      return [
        ...state,
        {
          id: action.id,
          text: action.text,
          completed: false
        }
      ]
    case 'TOGGLE_AUTHOR':
      return state.map(author =>
        (author.id === action.id)
          ? {...author, completed: !author.completed}
          : author
      )
    default:
      return state
  }
}

export default authorReducer

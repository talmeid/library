import { combineReducers } from 'redux'
import authorReducer from './authorReducer'
import visibilityFilter from './visibilityFilter'

export default combineReducers({
  authorReducer,
  visibilityFilter
})

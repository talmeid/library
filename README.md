Projeto feito com React através do comando create-react-app.

Utilizei o tutorial de Redux para uma todo list e está apenas listando os autores, faltando listar seus livros e realizar as operações de edição e exclusão de autor e livro.

Para inicializar, utilizar o comando npm start.
